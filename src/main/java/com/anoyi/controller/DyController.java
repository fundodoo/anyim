package com.anoyi.controller;

import com.anoyi.bean.DouyinUser;
import com.anoyi.controller.service.DyService;
import com.anoyi.mongo.repository.DouyinRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.util.List;

@Controller
@AllArgsConstructor
public class DyController {

    private final DouyinRepository douyinRepository;

    private final DyService dyService;

    @GetMapping("/dy/top")
    public String top(Model model){
        List<DouyinUser> douyinUsers = douyinRepository.findAll(Sort.by("rank"));
        model.addAttribute("users", douyinUsers);
        return "dy-top";
    }

    @GetMapping("/dy/v/{vid}")
    public String dyUserByVid(@PathVariable String vid, Model model) throws IOException {
        String uid = dyService.getUidByVid(vid);
        model.addAttribute("dyId", uid);
        return "dy-user";
    }

    @GetMapping("/dy/{id}")
    public String dyUser(@PathVariable String id, Model model){
        model.addAttribute("dyId", id);
        return "dy-user";
    }

}
