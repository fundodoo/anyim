package com.anoyi.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class ServiceController {

    @GetMapping("/service")
    public String service(){
        return "service";
    }

}
