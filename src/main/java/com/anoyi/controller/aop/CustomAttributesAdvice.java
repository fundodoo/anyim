package com.anoyi.controller.aop;

import com.anoyi.config.CustomAttributesConfig;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * 自定义属性
 */
@ControllerAdvice
@AllArgsConstructor
public class CustomAttributesAdvice {

    private final CustomAttributesConfig customAttributesConfig;

    /**
     * 全局 Model 中添加自定义属性
     */
    @ModelAttribute("custom")
    public CustomAttributesConfig customAttributes(){
        return customAttributesConfig;
    }

}
